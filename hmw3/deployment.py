
from prefect.deployments import DeploymentSpec
from prefect.orion.schemas.schedules import CronSchedule
from prefect.flow_runners import SubprocessFlowRunner
#from datetime import timedelta

DeploymentSpec(
    flow_location="homework.py",
    name="cron-9-15-deployment",
    #schedule=IntervalSchedule(interval=timedelta(minutes=5)),
    schedule=CronSchedule(cron="0 9 15 * *"),
    flow_runner=SubprocessFlowRunner(),
    tags=["ml","blah"]
)
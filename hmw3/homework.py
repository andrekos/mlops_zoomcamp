import pandas as pd
import mlflow
import pickle
from datetime import datetime

from sklearn.feature_extraction import DictVectorizer
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error

from prefect import flow, task, get_run_logger
from prefect.task_runners import SequentialTaskRunner


@task
def read_data(path):
    df = pd.read_parquet(path)
    return df

@task
def prepare_features(df, categorical, train=True):
    logger = get_run_logger()
    df['duration'] = df.dropOff_datetime - df.pickup_datetime
    df['duration'] = df.duration.dt.total_seconds() / 60
    df = df[(df.duration >= 1) & (df.duration <= 60)].copy()

    mean_duration = df.duration.mean()
    if train:
        # print(f"The mean duration of training is {mean_duration}")
        logger.info(f"The mean duration of training is {mean_duration}")
    else:
        # print(f"The mean duration of validation is {mean_duration}")
        logger.info(f"The mean duration of validation is {mean_duration}")
    
    df[categorical] = df[categorical].fillna(-1).astype('int').astype('str')
    return df

@task
def train_model(df, categorical):
    logger = get_run_logger()
    train_dicts = df[categorical].to_dict(orient='records')
    dv = DictVectorizer()
    X_train = dv.fit_transform(train_dicts) 
    y_train = df.duration.values

    #print(f"The shape of X_train is {X_train.shape}")
    #print(f"The DictVectorizer has {len(dv.feature_names_)} features")

    logger.info(f"The shape of X_train is {X_train.shape}")
    logger.info(f"The DictVectorizer has {len(dv.feature_names_)} features")

    lr = LinearRegression()
    lr.fit(X_train, y_train)
    y_pred = lr.predict(X_train)
    mse = mean_squared_error(y_train, y_pred, squared=False)
    # print(f"The MSE of training is: {mse}")
    logger.info(f"The MSE of training is: {mse}")
    return lr, dv

@task
def run_model(df, categorical, dv, lr):
    logger = get_run_logger()
    val_dicts = df[categorical].to_dict(orient='records')
    X_val = dv.transform(val_dicts) 
    y_pred = lr.predict(X_val)
    y_val = df.duration.values

    mse = mean_squared_error(y_val, y_pred, squared=False)
    # print(f"The MSE of validation is: {mse}")
    logger.info(f"The MSE of validation is: {mse}")
    mlflow.log_metric("rmse", mse)
    return

@task
def get_paths(date=None):
    from datetime import datetime
    from dateutil.relativedelta import relativedelta

    if date is None:
        date = datetime.now()
    else:
         date = datetime.fromisoformat(date)

    train_date = date - relativedelta(months=2)
    train_date = train_date.strftime('%Y-%m')
    train_path = './data/fhv_tripdata_' + train_date + '.parquet'
   
    valid_date = date - relativedelta(months=1)
    valid_date = valid_date.strftime('%Y-%m')
    valid_path = './data/fhv_tripdata_' + valid_date + '.parquet'
    return train_path, valid_path

@flow(task_runner=SequentialTaskRunner())
def main2(date="2021-08-15"):
    train_path, val_path = get_paths(date).result()
    
    mlflow.set_tracking_uri("sqlite:///mlflow.db")
    mlflow.set_experiment("nyc-taxi-experiment")

    categorical = ['PUlocationID', 'DOlocationID']

    df_train = read_data(train_path)
    df_train_processed = prepare_features(df_train, categorical)

    df_val = read_data(val_path)
    df_val_processed = prepare_features(df_val, categorical, False)

    # train the model
    lr, dv = train_model(df_train_processed, categorical).result()

    run_model(df_val_processed, categorical, dv, lr)

    # save model and artifacts 
    if isinstance(date, datetime):
        date = date.strftime('%Y-%m-%d')
    with open(f"dv-{date}.b", 'wb') as f:
        pickle.dump(dv, f)
    with open(f"model-{date}.bin", 'wb') as f:
        pickle.dump(lr, f)

main2()
